import re


def inc(lst):
    for c in reversed(range(len(lst))):
        lst[c] += 1
        if lst[c] > 25:
            lst[c] = 0
        else:
            return

def chk(lst):
    ok = False
    for c in range(0, len(lst) - 2):
        if lst[c] == lst[c + 1] - 1 == lst[c + 2] - 2:
            ok = True
            break
    if not ok:
        return None
    if any(x in ords for x in [ord(c) - 97 for c in 'iol']):
        return None
    str = ''.join([chr(c + 97) for c in ords])
    match = re.match(r".*(.)\1.*(.)\2.*", str)
    if match and match.group(1) != match.group(2):
        return str
    return None

pas = 'hepxxzaa'

ords = [ord(c) - 97 for c in pas]

print(ords)

while True:
    str = chk(ords)
    if str:
        break
    inc(ords)


print(str)