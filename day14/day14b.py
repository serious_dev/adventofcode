__author__ = 'BDrovnin'
str = """Vixen can fly 19 km/s for 7 seconds, but then must rest for 124 seconds.
Rudolph can fly 3 km/s for 15 seconds, but then must rest for 28 seconds.
Donner can fly 19 km/s for 9 seconds, but then must rest for 164 seconds.
Blitzen can fly 19 km/s for 9 seconds, but then must rest for 158 seconds.
Comet can fly 13 km/s for 7 seconds, but then must rest for 82 seconds.
Cupid can fly 25 km/s for 6 seconds, but then must rest for 145 seconds.
Dasher can fly 14 km/s for 3 seconds, but then must rest for 38 seconds.
Dancer can fly 3 km/s for 16 seconds, but then must rest for 37 seconds.
Prancer can fly 25 km/s for 6 seconds, but then must rest for 143 seconds."""
#               #3         #4                                 #5
# str = """Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
# Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds."""
info = []
sec = 2503
# sec = 1000
for l in str.splitlines():
    t = l.split()  # 7
    info.append(['fly', 0, 0] + list(map(int, (t[3], t[6], t[13]))) + [0])
print(info)
for i in range(sec):
    for o in info:
        o[1] += 1
        if o[0] == 'fly':
            o[2] += o[3]
            if o[1] == o[4]:
                o[0] = 'rest'
                o[1] = 0
        else:
            if o[1] == o[5]:
                o[0] = 'fly'
                o[1] = 0
                # print(info)
    maxDist = max([i[2] for i in info])
    for t in filter(lambda f: f[2] == maxDist, info):
        t[6] += 1
print(info)
print(max([i[6] for i in info]))
