__author__ = 'BDrovnin'
inp = """Sprinkles: capacity 2, durability 0, flavor -2, texture 0, calories 3
Butterscotch: capacity 0, durability 5, flavor -3, texture 0, calories 3
Chocolate: capacity 0, durability 0, flavor 5, texture -1, calories 8
Candy: capacity 0, durability -1, flavor 0, texture 5, calories 8"""
import re
import itertools

ing = [list(map(int, re.findall(r'(-?\d+).', x))) for x in inp.splitlines()]
print(ing)
perm = itertools.combinations_with_replacement(ing, 100)


def getTotals():
    for a in perm:
        t = 1
        for n in zip(*a):
            asum = sum(n)
            if asum < 0:
                asum = 0
            t *= asum
        yield t


print(max(getTotals()))
