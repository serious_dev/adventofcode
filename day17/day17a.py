__author__ = 'BDrovnin'
inp="""33
14
18
20
45
35
16
35
1
13
18
13
50
44
48
6
24
41
30
42"""
n = 150
import itertools
lines = list(map(int,inp.splitlines()))

c = {}
for x in range(len(lines)):
    for comb in itertools.combinations(lines,x):
        if sum(comb)==150:
            if len(comb) not in c:
                c[len(comb)] = 0
            else:
                c[len(comb)] += 1
print(c)