__author__ = 'BDrovnin'
field = []
mstr = input()
while mstr:
    field.append([0 if c == '.' else 1 for c in mstr])
    mstr = input()
# field = [[0 if c == '.' else 1 for c in line] for line in inp.splitlines()]
# print(field)
for i in range(100):
    newfield = []
    for y in range(len(field)):
        fline = [0 for j in range(len(field[0]))]
        for x in range(len(field[0])):
            newlist = []
            for t in [p for p in field[y - 1 if y > 0 else 0:y + 2]]:
                newlist += t[x - 1 if x > 0 else 0:x + 2]
            count = sum(newlist) - field[y][x]
            if count == 3 or (field[y][x] and count == 2):
                fline[x] = 1
        newfield.append(fline)
    field = newfield

print(sum([item for sublist in field for item in sublist]))
