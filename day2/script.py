from itertools import combinations

s = input()
n = 0
while s:
    def f(t): return map(lambda x: int(x[0]) * int(x[1]), combinations(t.split('x'), 2))


    n += sum(f(s)) * 2 + min(f(s))
    s = input()
print(n)
