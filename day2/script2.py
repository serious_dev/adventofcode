from functools import reduce

s = input()
n = 0
while s:
    m = sorted(map(int, s.split('x')))[0:2]
    n += m[0] * 2 + m[1] * 2 + reduce(lambda res, x: res * int(x), s.split('x'), 1)
    s = input()
print(n)
