def getl():
    val = (0, 0)
    yield val
    for y in [{'<': (1, 0), '>': (-1, 0), 'v': (0, 1), '^': (0, -1)}[x] for x in input()]:
        val = (val[0] + y[0], val[1] + y[1])
        yield val


print(len(set(getl())))
