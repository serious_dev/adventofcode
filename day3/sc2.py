def getl(elf):
    pos = (0, 0)
    yield pos
    for y in [{'<': (1, 0), '>': (-1, 0), 'v': (0, 1), '^': (0, -1)}[x] for x in elf]:
        pos = (pos[0] + y[0], pos[1] + y[1])
        yield pos


elf = input()

print(len(set(getl(elf[0::2])) | set(getl(elf[1::2]))))
