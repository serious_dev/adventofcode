import re

nice = 0
while True:
    s = input()
    if not s:
        break
    if not re.search(r"(..).*\1", s):
        continue
    if not re.search(r"(.).\1", s):
        continue
    nice += 1
print(nice)
