v = 0
n = 1000
a = [[v] * n for x in range(n)]
# print(a)
while True:
    s = input()
    if not s: break
    args = s.rsplit(maxsplit=3)
    begin = eval(args[1])
    end = eval(args[3])
    com = args[0]
    for x in range(begin[0], end[0] + 1):
        for y in range(begin[1], end[1] + 1):
            if com == 'turn on':
                a[x][y] += 1
            elif com == 'turn off':
                if a[x][y] > 0:
                    a[x][y] -= 1
            else:
                a[x][y] += 2
count = 0
for l in a:
    count += sum(l)

print(count)
