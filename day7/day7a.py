d = {}
cache = {}


def doOp(op):
    if op in cache:
        return cache[op]

    if op.isdigit():
        return int(op)
    if d[op].isdigit():
        return int(d[op])

    sp = d[op].split()
    if 'NOT' in d[op]:
        return (~ doOp(sp[1])) & 65535
    elif 'AND' in d[op]:
        val = doOp(sp[0]) & doOp(sp[2])
        cache[op] = val
        return val
    elif 'OR' in d[op]:
        val = doOp(sp[0]) | doOp(sp[2])
        cache[op] = val
        return val
    elif 'LSHIFT' in d[op]:
        val = (doOp(sp[0]) << doOp(sp[2])) & 65535
        cache[op] = val
        return val
    elif 'RSHIFT' in d[op]:
        val = doOp(sp[0]) >> doOp(sp[2])
        cache[op] = val
        return val
    else:
        return doOp(d[op])


t = input()
while t:
    ar = t.rsplit(maxsplit=2)
    d[ar[2]] = ar[0]
    t = input()
d['b'] = '16076'
print(d)
e = doOp('a')
print(e)
print(cache)
