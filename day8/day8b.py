t = input()
ans = 0
while t:
    ans += len(t.replace("\\", "\\\\").replace('"', r'\"')) + 2 - len(t)
    t = input()
print(ans)
